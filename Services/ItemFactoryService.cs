﻿using csharp.Common;
using csharp.Models;

namespace csharp.Services
{
    public class ItemFactoryService
    {
        public static Item CreateItem(string name, int sellIn, int quality)
        {
            switch (name)
            {
                case Constants.AgedBrie:
                    return new AgedBrieItem(sellIn, quality);
                case Constants.Backstage:
                    return new BackstageItem(sellIn, quality);
                case Constants.Sulfuras:
                    return new SulfurasItem(sellIn, quality);
                case Constants.Conjured:
                    return new ConjuredItem(sellIn, quality);
                default:
                    return new StandardItem(name, sellIn, quality);
            }
        }
    }
}
