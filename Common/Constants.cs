﻿namespace csharp.Common
{
    public static class Constants
    {
        public const string AgedBrie = "Aged Brie";

        public const string Backstage = "Backstage passes to a TAFKAL80ETC concert";

        public const string Sulfuras = "Sulfuras, Hand of Ragnaros";

        public const string Conjured = "Conjured Mana Cake";
    }
}
