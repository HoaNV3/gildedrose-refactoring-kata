﻿using System;
using System.Collections.Generic;
using csharp.Common;
using csharp.Models;
using csharp.Services;

namespace csharp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("OMGHAI!");

            IList<Item> Items = new List<Item>{
                ItemFactoryService.CreateItem("+5 Dexterity Vest", 10, 20),
                ItemFactoryService.CreateItem(Constants.AgedBrie, 2, 0),
                ItemFactoryService.CreateItem("Elixir of the Mongoose", 5, 7),
                ItemFactoryService.CreateItem(Constants.Sulfuras, 0, 80),
                ItemFactoryService.CreateItem(Constants.Sulfuras, -1, 80),
                ItemFactoryService.CreateItem(Constants.Backstage, 15, 20),
                ItemFactoryService.CreateItem(Constants.Backstage, 10, 49),
                ItemFactoryService.CreateItem(Constants.Backstage, 5, 49),
                ItemFactoryService.CreateItem(Constants.Conjured, 3, 6),
            };

            var app = new GildedRose(Items);


            for (var i = 0; i < 31; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                foreach (var item in Items)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine("");
                app.UpdateQuality();
            }

            Console.ReadLine();
        }
    }
}
