﻿using NUnit.Framework;
using System.Collections.Generic;
using csharp.Common;
using csharp.Models;
using csharp.Services;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        public void AgedBrie()
        {
            IList<Item> Items = new List<Item> {  ItemFactoryService.CreateItem(Constants.AgedBrie, 0, 0) };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(Constants.AgedBrie, Items[0].Name);
        }
    }
}
