﻿using csharp.Common;

namespace csharp.Models
{
    internal class AgedBrieItem : Item
    {
        public AgedBrieItem(int sellIn, int quality)
            : base(Constants.AgedBrie, sellIn, quality)
        {
        }

        public override void UpdateItemInfo()
        {
            if (Quality < 50)
            {
                Quality += 1;
            }

            UpdateSellIn();

            if (SellIn < 0 && Quality < 50)
            {
                Quality += 1;
            }
        }
    }
}
