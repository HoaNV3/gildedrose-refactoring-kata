﻿namespace csharp.Models
{
    public class StandardItem : Item
    {
        public StandardItem(string name, int sellIn, int quality) 
            : base(name, sellIn, quality)
        {
        }

        public override void UpdateItemInfo()
        {
            UpdateSellIn();
            Quality -= SellIn >= 0 ? 1 : 2;
            base.UpdateItemInfo();
        }
    }
}
