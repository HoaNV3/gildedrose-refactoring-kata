﻿using csharp.Common;

namespace csharp.Models
{
    public class BackstageItem : Item
    {
        public BackstageItem(int sellIn, int quality)
            : base(Constants.Backstage, sellIn, quality)
        {
        }

        public override void UpdateItemInfo()
        {
            if (Quality < 50)
            {
                Quality += 1;

                if (SellIn < 11 && Quality < 50)
                {
                    Quality += 1;
                }

                if (SellIn < 6 && Quality < 50)
                {
                    Quality += 1;
                }
            }

            UpdateSellIn();

            if (SellIn < 0)
            {
                Quality -= Quality;
            }
        }
    }
}
