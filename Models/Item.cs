﻿using csharp.Common;

namespace csharp.Models
{
    public class Item
    {
        public Item(string name, int sellIn, int quality)
        {
            Name = name;
            SellIn = sellIn;
            Quality = quality;
        }

        public string Name { get; set; }
        public int SellIn { get; set; }
        public int Quality { get; set; }

        protected void UpdateSellIn()
        {
            SellIn -= 1;
        }

        public virtual void UpdateItemInfo()
        {
            if (Quality < 0)
            {
                Quality = 0;
            }

            if (Quality > 50)
            {
                Quality = 50;
            }
        }

        public override string ToString()
        {
            return Name + ", " + SellIn + ", " + Quality;
        }  
    }
}
