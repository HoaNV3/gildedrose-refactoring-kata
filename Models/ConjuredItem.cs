﻿using csharp.Common;

namespace csharp.Models
{
    public class ConjuredItem : Item
    {
        public ConjuredItem(int sellIn, int quality)
            : base(Constants.Conjured, sellIn, quality)
        {
        }

        public override void UpdateItemInfo()
        {
            // "Conjured" items degrade in Quality twice as fast as normal items
            UpdateSellIn();
            Quality -= SellIn >= 0 ? 1 : 2;
            base.UpdateItemInfo();
        }
    }
}
