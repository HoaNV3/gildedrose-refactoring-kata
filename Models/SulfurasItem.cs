﻿using csharp.Common;

namespace csharp.Models
{
    public class SulfurasItem : Item
    {
        public SulfurasItem(int sellIn, int quality) 
            : base(Constants.Sulfuras, sellIn, quality)
        {
        }

        public override void UpdateItemInfo()
        {
        }
    }
}
